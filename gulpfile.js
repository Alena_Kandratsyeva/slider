var gulp = require('gulp');
var less = require('gulp-less');
var svgSprite = require('gulp-svg-sprite');

gulp.task('default', function(){
    gulp.watch('pre-less/components/*.less', ['less']);
    //gulp.watch('pre-less/components/*.less', ['less']);
});

gulp.task('less', function(){
    gulp.src('pre-less/pre-less.less')
        .pipe(less())
        .pipe(gulp.dest('css'))
});

config					= {
    mode				: {
        css				: {		// Activate the «css» mode
            render		: {
                css		: true	// Activate CSS output (with default options)
            }
        }
    }
};

gulp.task('sprite', function(){
    gulp.src('img/sprite/*.svg')
    .pipe(svgSprite( config ))
    .pipe(gulp.dest('css/svg'));
});








