$(document).ready(function() {

    var $s_left = $(".left");
    var $s_right = $(".right");
    var $img_count = $(".images").children().length;
    var $slide_area =  $(".images");
    var $pages = $(".pages").children("li");
    var $img_width = 590;
    $img_count -=1;

    $(".pages").children("li").eq(0).addClass("active");

    $s_right.click(function( event ){
        var $current_margin = +$slide_area.css("margin-left").slice(0,-2);
         if( $current_margin == ( -($img_count)*$img_width)  ){
             $slide_area.animate({"marginLeft" : "0px"}, 500 );
             $pages.removeClass("active");
             $pages.eq(0).addClass("active");
         }
         else {
             $slide_area.animate({marginLeft: ($current_margin - $img_width)}, 500);
             //расчитать номер кружка
             var $circle_number = (-1)*($current_margin/$img_width) +1;
             $pages.removeClass("active");
             $pages.eq($circle_number).addClass("active");
         }
    });


    $s_left.click(function( event ){
        var $current_margin = +$slide_area.css("margin-left").slice(0,-2);
        if( $current_margin == 0){
            $slide_area.animate({"marginLeft": -(($img_count)*$img_width)}, 500 );
            $pages.removeClass("active");
            $pages.last().addClass("active");
        }
        else{
            $slide_area.animate({"marginLeft": ($current_margin + $img_width)}, 500 );
            var $circle_number = (-1)*($current_margin/$img_width) - 1;
            $pages.removeClass("active");
            $pages.eq($circle_number).addClass("active");
        }
    });


    $('ul.pages').on('click', 'li', function() {
        var $circle_num = $( this ).index();
        $pages.removeClass("active");
        $pages.eq($circle_num).addClass("active");
        var $margin = $circle_num*$img_width;
        $slide_area.animate({marginLeft: (-1*$margin)}, 500);

    });
});
